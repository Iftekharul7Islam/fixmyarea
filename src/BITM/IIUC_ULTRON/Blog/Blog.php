<?php
namespace App\Blog;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class Blog extends DB{
    public $id="";
    public $title="";
    public $author="";
	public $image="";
	public $description="";

    public function __construct(){
    parent:: __construct();
    if(!isset( $_SESSION)) session_start();
}

    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }

        if(array_key_exists('title',$data)){
            $this->title = $data['title'];
        }
		if(array_key_exists('author',$data)){
            $this->author = $data['author'];
        }
		
		if(array_key_exists('image',$data)){
            $this->image = $data['image'];
        }
		 
		if(array_key_exists('description',$data)){
            $this->description = $data['description'];
        }
		 
    }


    public function store()
    {

        $arrData = array($this->title, $this->author, $this->image,$this->description
            );

        $sql = "INSERT INTO blog_posts ( title, author, image, description) VALUES ( ?, ?,? ,?)";

        $STH = $this->DBH->prepare($sql);


        $result = $STH->execute($arrData);
        // return var_dump($result);
        if ($result)
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Has Been Inserted Successfully!</h3></div>");
        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Has not Been Inserted Successfully!</h3></div>");


        Utility::redirect('manage-blog.php');

    }

   
   

    public function index($mode="ASSOC"){
        $mode = strtoupper($mode);
        $STH = $this->DBH->query('SELECT * from blog_posts ORDER BY id DESC ');

        if($mode=="OBJ")
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData = $STH->fetchAll();

        return $arrAllData;

    }
	
	
		 
		 //all reports
		 
		 public function all_reports_new($mode="ASSOC"){
        $mode = strtoupper($mode);
        $STH = $this->DBH->query('SELECT * from blog_posts where admin_feedback="" and  is_fixed=0 and ward_no='.$this->ward_no);

        if($mode=="OBJ")
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData = $STH->fetchAll();

        return $arrAllData;

         }
		 


	
	    public function view($mode="ASSOC"){
		
		$mode = strtoupper($mode);
        $STH = $this->DBH->query('SELECT * from blog_posts where id='.$this->id);

        if($mode=="OBJ")
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
		
        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }// end of view()
	
		public function indexPaginator($page=0,$itemsPerPage=3){
        
        $start = (($page-1) * $itemsPerPage);
        
        $sql = "SELECT * from blog_posts  LIMIT $start,$itemsPerPage";
        
        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
        
    }// end of indexPaginator();
	
			public function indexPaginator_fixed($page=0,$itemsPerPage=3){
        
        $start = (($page-1) * $itemsPerPage);
        
        $sql = "SELECT * from blog_posts  LIMIT $start,$itemsPerPage";
        
        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
        
    }// end of indexPaginator();
	
	
	 public function search($requestArray){
        $sql = "";
        if( isset($requestArray['title']) && isset($requestArray['description']) )  $sql = "SELECT * FROM `reports`  (`title` LIKE '%".$requestArray['search']."%' OR `ward_no` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['title']) && !isset($requestArray['description']) ) $sql = "SELECT * FROM `reports`  `title` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['title']) && isset($requestArray['description']) )  $sql = "SELECT * FROM `reports`  `description` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;
    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `reports` ";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->area);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->area);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->ward_no);
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->ward_no);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords
	
	
	//========================ADMIN Secion STARTS=======================
	
	    public function update(){

     
        $sql = 'UPDATE reports SET is_fixed = 1 where id ='.$this->id;
		
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('view_reports.php');


    }// end of update()
	
		    public function update_feedback(){
		
		 $arrData  = array($this->admin_feedback);

        $sql = 'UPDATE reports  SET admin_feedback  = ?  where id ='.$this->id;    
		
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('view_reports.php');


    }// end of update()
	
	    public function delete(){


        $sql = "Delete from blog_posts where id=".$this->id;

        $STH = $this->DBH->prepare($sql);


        $result = $STH->execute();

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Deleted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Deleted Successfully!</h3></div>");

        Utility::redirect('manage-blog.php');



    }// end of delete

    public function delete_multiple(){

    $arr = explode(" ",$this->del_id );
        foreach ($arr as $id){

            $sql = "Delete from blog_posts where id=".$id;
            $STH = $this->DBH->prepare($sql);
            $result = $STH->execute();
        }


        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Deleted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Deleted Successfully!</h3></div>");

        Utility::redirect('view_reports.php');

    } // end of multiple delete
	
	


}

?>

