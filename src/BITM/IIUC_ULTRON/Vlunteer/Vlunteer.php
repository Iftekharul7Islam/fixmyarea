<?php
namespace App\Vlunteer;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class Vlunteer extends DB
{
    public $id = "";
    public $vlunteer_group_name = "";
    public $website = "";
    public $address = "";
    public $phone = "";
    public $contact_person_name = "";
    public $contact_person_mobile_no = "";
    public $doc_file = "";
    public $email = "";

    public $username = "";
    public $password = "";
    public $usertype = "volunteer";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data = array())
    {

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

        if (array_key_exists('vlunteer_group_name', $data)) {
            $this->vlunteer_group_name = $data['vlunteer_group_name'];
        }
        if (array_key_exists('website', $data)) {
            $this->website = $data['website'];
        }
        if (array_key_exists('address', $data)) {
            $this->address = $data['address'];
        }
        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
        }
        if (array_key_exists('contact_person_name', $data)) {
            $this->contact_person_name = $data['contact_person_name'];
        }
        if (array_key_exists('contact_person_mobile_no', $data)) {
            $this->contact_person_mobile_no = $data['contact_person_mobile_no'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('doc_file', $data)) {
            $doc_file = time() . $_FILES['doc_file']['name'];
            $temporary_location = $_FILES['doc_file']['tmp_name'];
            move_uploaded_file($temporary_location, '../../../resource/vlunteer/' . $doc_file);
            $this->doc_file = $doc_file;
        }
        if (array_key_exists('username', $data)) {
            $this->username = $data['username'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = md5($data['password']);
        }
        if (array_key_exists('usertype', $data)) {
            $this->usertype = $data['usertype'];
        }

        return $this;
    }

    public function store()
    {
        $arrData = array($this->vlunteer_group_name, $this->phone, $this->website, $this->address, $this->contact_person_name, $this->contact_person_mobile_no, $this->doc_file, $this->email);

        $sql = "INSERT INTO vlunteers (vlunteer_group_name,phone,website,address,contact_person_name,contact_person_mobile_no,doc_file,email) VALUES (?,?,?,?,?,?,?,?)";

        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Registration has been completed successfully, a confirmation email will be sent soon.
                </div>");
            Utility::redirect('join_us.php');
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            Utility::redirect('join_us.php');
        }
    }


    public function index($mode = "ASSOC")
    {
        $mode = strtoupper($mode);
        $STH = $this->DBH->query('SELECT * from vlunteers where is_approved=0 ');

        if ($mode == "OBJ")
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData = $STH->fetchAll();

        return $arrAllData;

    }

    public function index_all($mode = "ASSOC")
    {
        $mode = strtoupper($mode);
        $STH = $this->DBH->query('SELECT * from vlunteers where is_approved=1');

        if ($mode == "OBJ")
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData = $STH->fetchAll();

        return $arrAllData;

    }

    public function change_password()
    {
        $query = "UPDATE `vlunteers` SET `user_pass`=:password  WHERE `user_info`.`email` =:email";
        $result = $this->DBH->prepare($query);
        $result->execute(array(':password' => $this->password1, ':email' => $this->email));
        /*
                if ($result) {
                    Message::message("
                     <div class=\"alert alert-info\">
                     <strong>Success!</strong> Password has been updated successfully.
                      </div>");
                    Utility::redirect('signup_login.php');
                } else {
                    echo "Error";
                }*/
    }

    public function view()
    {
        $query = "SELECT * FROM `vlunteers` WHERE `vlunteers`.`id` =:id";
        $result = $this->DBH->prepare($query);
        $result->execute(array(':id' => $this->id));
        $row = $result->fetch(PDO::FETCH_OBJ);
        return $row;
        unset($_FILES);
    }// end of view()


    public function validTokenUpdate()
    {
        $query = "UPDATE `user-management`.`users` SET  `email_verified`='" . 'Yes' . "' WHERE `users`.`email` =:email";
        $result = $this->DBH->prepare($query);
        $result->execute(array(':email' => $this->email));

        if ($result) {
            Message::message("
             <div class=\"alert alert-success\">
             <strong>Success!</strong> Email verification has been successful. Please login now!
              </div>");
        } else {
            echo "Error";
        }
        return Utility::redirect('../../../../views/SEIPXXXX/User/Profile/signup.php');
    }

    public function update()
    {
        $arrData = array($this->vlunteer_group_name, $this->phone, $this->website, $this->mobile, $this->address, $this->contact_person_name, $this->contact_person_mobile_no, $this->doc_file, $this->email);

        $query = 'UPDATE vlunteers  SET vlunteer_group_name=?,phone=?,website=?,mobile=?,address=?,contact_person_name=?,contact_person_mobile_no=?,doc_file=?,email=? WHERE id =\'' . $this->id . '\'';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);
        //var_dump($STH);
        //die();
        if ($result) {
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Data has been updated  successfully.
              </div>");
        } else {
            echo "Error";
        }
        return Utility::redirect($_SERVER['HTTP_REFERER']);
    }


    public function member_accept()
    {
        //admin
        $arrData = array($this->username, $this->password, $this->usertype);
        //return var_dump($arrData);
        $sql = "INSERT INTO admin (adminname,adminpass,user_type) VALUES (?,?,?)";

        $STH = $this->DBH->prepare($sql);
         $result = $STH->execute($arrData);

        //approve
        $sql = 'UPDATE vlunteers SET is_approved = 1 where id =' . $this->id;

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute();

        if ($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('vlunteer_request.php');
    }


    public function delete()
    {


        $sql = "Delete from vlunteers where id=" . $this->id;

        $STH = $this->DBH->prepare($sql);


        $result = $STH->execute();

        if ($result) {
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Data has been updated  successfully.
              </div>");
        } else {
            echo "Error";
        }

        Utility::redirect('vlunteer_request.php');


    }// end of delete

    public function getEmail()
    {
        $query = "SELECT email,contact_person_mobile_no FROM `vlunteers` WHERE `vlunteers`.`id` =$this->id";
        $result = $this->DBH->prepare($query);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_OBJ);
        return $row;
    }

}

