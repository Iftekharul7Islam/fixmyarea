<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
require_once("../../../vendor/autoload.php");
use App\Reports\Reports;
use App\User\User;
use App\Vlunteer\Vlunteer;
use App\Admin\Auth;
use App\Message\Message;
use App\Utility\Utility;

if (!isset($_SESSION)) session_start();
$_GLOBAL = Message::message();

 $objReports = new Reports();
 $allData = $objReports->index("obj");
 $new_reports=0;
 foreach ($allData as $oneData) {  
 $new_reports=$new_reports+1;
 }

 $objUsers = new User();
 $allData = $objUsers->index("obj");
 $new_member=0;
 foreach ($allData as $oneData) {  
 $new_member=$new_member+1;
 }
 
 $objUsers = new User();
 $allData = $objUsers->index_all("obj");
 $all_member=0;
 foreach ($allData as $oneData) {  
 $all_member=$all_member+1;
 }
 
 $objVlunteer = new Vlunteer();
 $allData = $objVlunteer->index("obj");
 $new_vlunteer=0;
 foreach ($allData as $oneData) {  
 $new_vlunteer += 1;
 }

 if (!isset($_SESSION)) session_start();
 $_GLOBAL = Message::message();

 if (isset($_SESSION['adminname'])) {
    ?>

<!DOCTYPE HTML>
<html>
<head>
<title>CleanCity</title>
	<link href="../../../resource/css/form.css" rel="stylesheet">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
			

<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link href="../../../resource/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- start plugins -->
<script type="text/javascript" src="../../../resource/js/jquery.min.js"></script>

<!-- start slider -->
<style type="text/css">
  h2{
    color: white;
  }
  a{
    text-decoration: none;
  }
</style>

</head>
<body>
<div class="header_bg1">
<div class="container">
	<div class="row header">
		<div class="logo navbar-left">
			<h1><a href="../index.php">Clean City</a></h1>
		</div>
		<div class="h_search navbar-right">
			<form id="searchForm"  action="problems.php"  method="get">			   
               <input type="hidden" name="area" id="inlineCheckbox1" checked="" value="">                                				
               <input type="hidden" name="ward_no" id="inlineCheckbox1" checked="" value="">                                                                        
			    <input type="text" id="searchID" name="search" class="text" value="Search by Area Name or Ward No" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search by Area Name or Ward No';}">
				<input type="submit"  value="search">
			</form>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php include('header.php'); ?>
	<div class="clearfix"></div>
</div>
</div>
<div class="container">
<!-- start main -->

	<div>
		<h1></b><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Welcome: </b><i><?php echo $_SESSION['adminname'] ?> </i><h1> 
		
	</div>
	


<div class="row">
	
  <?php if($_SESSION['user_type']=='superadmin'){ ?>
    <div class="col-sm-4" style="background-color:#D91E18; text-align: center;color:white;height: 100px">
      <a href="view_reports.php">
    	<h2><span class="glyphicon glyphicon-edit"></span> New Reports(<?php echo $new_reports;?>)</h2>
      </a>
    </div>
    
    
    
    <div class="col-sm-4" style="background-color:#663399; text-align: center;color:white;height: 100px">
      <a href="member_request.php">
		<h2><span class="glyphicon glyphicon-user"></span> Member Requests(<?php echo $new_member;?>)</h2>
    </a>
    </div>
    
    
    
    <div class="col-sm-4" style="background-color:#87D37C; text-align: center;color:white;height: 100px">
      <a href="member_all.php">
    	<h2><span class="glyphicon glyphicon-list"></span> All Members(<?php echo $all_member;?>)</h2>
      </a>
    </div>

    <div class="col-sm-4" style="background-color:#F4B350; text-align: center;color:white;height: 100px">
      <a href="vlunteer_request.php">
    	<h2><span class="glyphicon glyphicon-list"></span> Volunteer Requests(<?php echo $new_vlunteer;?>)</h2>
     </a>
    </div>

    
    <div class="col-sm-4" style="background-color:#52B3D9; text-align: center;color:white; height: 100px">
      <a href="vlunteer_all.php">
    	<h2><span class="glyphicon glyphicon-list"></span> All Volunteer</h2>
      </a>
    </div>

      <?php } ?>
    
 </div>


<!-- end main -->
<!-- <div class="footer_bg">
	<div class="container">
		<div class="row  footer">
			<div class="copy text-center">
				<p class="link"><span>&copy; Developed by <a href="http://facebook.com/fahim.iftekhar.5">Iftekharul Islam</a></span></p>
			</div>
		</div>
	</div>
</div> -->
</body>
</html>
<?php } else {
    Message::message("
                <div class=\"alert alert-success\">
                            <strong>Failed!</strong> Please Log in first.
                </div>");
    Utility::redirect('admin_login.php');
}
?>
