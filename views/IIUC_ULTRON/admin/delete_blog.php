<?php
include_once('../../../vendor/autoload.php');
use App\Blog\Blog;

$objBlog = new Blog();

if(isset($_GET["id"])) {
    $objBlog->setData($_GET);
    $objBlog->delete();
}

elseif(isset($_POST["del_id"]))
{
    $objBlog->setData($_POST);
    $objBlog->delete_multiple();

}

