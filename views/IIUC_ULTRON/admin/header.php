<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
    .fa{
        font-size: 16px;
    }
</style>
<div class="row h_menu">
        <nav class="navbar navbar-default navbar-left" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li><a href="admin_index.php"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="manage-blog.php"><i class="fa fa-newspaper-o"></i> Blog</a></li> 
                <li><a href="../index.php" target="_blank"><i class="fa fa-globe"></i> View website</a></li>
                <li><a href="logout.php"><i class="fa fa-sign-out"></i> Logout</a></li>            
              </ul>
            </div>
            <!-- start soc_icons -->
        </nav>
        <div class="soc_icons navbar-right">
                
        </div>
    </div>