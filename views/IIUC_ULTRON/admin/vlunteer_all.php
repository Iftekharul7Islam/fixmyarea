<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
require_once("../../../vendor/autoload.php");
use App\Reports\Reports;
use App\Vlunteer\Vlunteer;
use App\Admin\Auth;
use App\Message\Message;
use App\Utility\Utility;

if (!isset($_SESSION)) session_start();
$_GLOBAL = Message::message();

if (isset($_SESSION['adminname'])) {
?>
<!DOCTYPE HTML>
<html>
<head>
<title>CleanCity</title>
    <link href="../../../resource/css/form.css" rel="stylesheet">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            

<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link href="../../../resource/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- start plugins -->
<script type="text/javascript" src="../../../resource/js/jquery.min.js"></script>

<!-- start slider -->
    <style>
#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform: scale(0)} 
    to {-webkit-transform: scale(1)}
}

@keyframes zoom {
    from {transform: scale(0.1)} 
    to {transform: scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
    </style>
        <style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td ,tr{
    text-align: center;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #000711;
    color: white;
}
</style>

</head>
<body>
<div class="header_bg1">
<div class="container">
    <div class="row header">
        <div class="logo navbar-left">
            <h1><a href="../index.php">Clean City</a></h1>
        </div>
        <div class="h_search navbar-right">
            <form id="searchForm"  action="../problems.php"  method="get">             
               <input type="hidden" name="area" id="inlineCheckbox1" checked="" value="">                                               
               <input type="hidden" name="ward_no" id="inlineCheckbox1" checked="" value="">                                                                        
                <input type="text" id="searchID" name="search" class="text" value="Search by Area Name or Ward No" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search by Area Name or Ward No';}">
                <input type="submit"  value="search">
            </form>
        </div>
        <div class="clearfix"></div>
    </div>
    <?php include('header.php'); ?>
    <div class="clearfix"></div>
</div>
</div>
<div class="container">
<!-- start main -->

<h2 style="text-align:center">Volunteer All</h2>
<div style="">
<table> 
    
    
    <tr >
        <th>Id</th>
        <th>Org Name</th>
        <th>Email</th>
        <th>Website</th>
        <th>Address</th>
        <th>Contact person</th>
        <th>Mobile</th> 
        <th>Doc</th>    
        <th>Approval</th>
        
    </tr>

<?php 
            $objVlunteer = new Vlunteer();
            $allData = $objVlunteer->index_all("obj");
            foreach ($allData as $oneData) { ?>
       <tr>
        <td style="text-align:center"><?php echo $oneData->id; ?></td>
        <td style="text-align:center"><?php echo $oneData->vlunteer_group_name; ?></td>
        <td style="text-align:center"><?php echo $oneData->email; ?></td>   
        <td><?php echo $oneData->website; ?></td>
        <td><?php echo $oneData->address; ?></td>
        <td><?php echo $oneData->contact_person_name; ?></td>
        <td><?php echo $oneData->contact_person_mobile_no; ?></td>
        <td><a href="../../../resource/vlunteer/<?php echo $oneData->doc_file; ?>" download>View</a></td>
        <td>
        <a href="vlunteer_delete.php?id=<?php echo $oneData->id ?>" onclick="return confirm('Are you sure you want to delete this item?');"  class="btn btn-danger" role="button"><span class="glyphicon glyphicon - trash"></span> Remove</a>
        </td> 
        
        
    </tr>

<?php } ?>
<table> 

    </div>
<!-- end main -->

<!-- The Modal -->
<div id="myModal" class="modal">
  <span class="close">×</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>
<style>
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
</style>
</body>
</html>
<?php } else {
    Message::message("
                <div class=\"alert alert-success\">
                            <strong>Failed!</strong> Please Log in first.
                </div>");
    Utility::redirect('admin_login.php');
}
?>
