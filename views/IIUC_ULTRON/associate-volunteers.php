<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
require_once("../../vendor/autoload.php");
use App\Vlunteer\Vlunteer;
use App\Message\Message;

if (!isset($_SESSION)) session_start();
$_GLOBAL = Message::message();
?>

<!DOCTYPE HTML>
<html>
<head>
    <title>CleanCity</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="../../resource/css/bootstrap.css" rel='stylesheet' type='text/css'/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="../../resource/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- start plugins -->
    <script type="text/javascript" src="../../resource/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../resource/js/bootstrap.js"></script>
    <!-- start slider -->

    <!----font-Awesome----->
    <link rel="stylesheet" href="../../resource/fonts/css/font-awesome.min.css">
    <!----font-Awesome----->
    <link rel="stylesheet" href="../../resource/css/flexslider.css" type="text/css" media="screen"/>
	<!-- Scroll to top-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://arrow.scrolltotop.com/arrow7.js"></script>
    <noscript>Not seeing a <a href="http://www.scrolltotop.com/">Scroll to Top Button</a>? Go to our FAQ page for more info.</noscript>
	<!-- Scroll to top End-->
    <style type="text/css">
        table{
            font-size:20px;
        }
    </style>
</head>
<body>
<div class="header_bg1">
    <div class="container">
        <div class="row header">
            <div class="logo navbar-left">
                <h1><a href="index.php">Clean City</a></h1>
            </div>
            <div class="h_search navbar-right">
                <form id="searchForm" action="problems.php" method="get">
                    <input type="hidden" name="area" id="inlineCheckbox1" checked="" value="">
                    <input type="hidden" name="ward_no" id="inlineCheckbox1" checked="" value="">
                    <input type="text" id="searchID" name="search" class="text" value="Search by Area Name or Ward No"
                           onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search by Area Name or Ward No';}">
                    <input type="submit" value="search">
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
        <?php include('header.php'); ?>
        <div class="clearfix"></div>
    </div>
</div>
<div class="main_bg"><!-- start main -->
    <div class="container">
        <div class="technology row">
            <h2>Associate volunteer Group List</h2>

            <?php

            $objVlunteer = new Vlunteer();
            $allData = $objVlunteer->index_all("obj");

        
            ?>
            <table width="100%" class="table table-striped"> 
    
                
                <tr >
                    
                    <th>Org Name</th>
                    <th>Email</th>
                    <th>Website</th>
                    <th>Address</th>
                    <th>Contact person</th>
                    <th>Mobile</th> 
                    <th>Doc</th>    
               
                    
                </tr>

            <?php 
                    
                        foreach ($allData as $oneData) { ?>
                   <tr>
                    
                    <td ><?php echo $oneData->vlunteer_group_name; ?></td>
                    <td ><?php echo $oneData->email; ?></td>   
                    <td><?php echo $oneData->website; ?></td>
                    <td><?php echo $oneData->address; ?></td>
                    <td><?php echo $oneData->contact_person_name; ?></td>
                    <td><?php echo $oneData->contact_person_mobile_no; ?></td>
                    <td><a href="../../../resource/vlunteer/<?php echo $oneData->doc_file; ?>" download>View</a></td>
                  
                    </td> 
                    
                    
                </tr>

            <?php } ?>
            <table> 

            

           
           


            <!-- <div class="alert alert-warning alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong>Warning!</strong> Better check yourself, you're not looking too good.
            </div> -->
        </div>
       
    </div>
</div><!-- end main -->







</body>
</html>