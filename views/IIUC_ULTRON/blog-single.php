<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
require_once("../../vendor/autoload.php");
use App\Blog\Blog;
use App\Message\Message;

if (!isset($_SESSION)) session_start();
$_GLOBAL = Message::message();
?>

<!DOCTYPE HTML>
<html>
<head>
    <title>CleanCity</title>
    <link href="../../resource/css/form.css" rel="stylesheet">
<!--
    <script type="text/javascript">
        if (screen.width <= 699) {
            document.location = "single_view_m.php";
        }
    </script> -->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="../../resource/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- start plugins -->
    <script type="text/javascript" src="../../resource/js/jquery.min.js"></script>

    <!-- start slider -->
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <!-- Scroll to top-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://arrow.scrolltotop.com/arrow7.js"></script>
    <noscript>Not seeing a <a href="http://www.scrolltotop.com/">Scroll to Top Button</a>? Go to our FAQ page for more
        info.
    </noscript>
    <!-- Scroll to top End-->
	
	<!--Facebook share -->
    <meta property="og:url"           content="http://localhost/FixMyArea_BITM_FINAL_PROJECT/views/IIUC_ULTRON/index.php" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="FixMyArea" />
	<meta property="og:description"   content="description" />
	<meta property="og:image"         content="" />
<!--Facebook share -->


</head>
<body>
<div class="header_bg1">
    <div class="container">
        <div class="row header">
            <div class="logo navbar-left">
                <h1><a href="index.php">Clean City</a></h1>
            </div>
            <div class="h_search navbar-right">
                <form id="searchForm" action="problems.php" method="get">
                    <input type="hidden" name="area" id="inlineCheckbox1" checked="" value="">
                    <input type="hidden" name="ward_no" id="inlineCheckbox1" checked="" value="">
                    <input type="text" id="searchID" name="search" class="text" value="Search by Area Name or Ward No"
                           onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search by Area Name or Ward No';}">
                    <input type="submit" value="search">
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
        <?php include('header.php'); ?>
        <div class="clearfix"></div>
    </div>
</div>
<div class="container"><!-- start main -->

    <!---->
    <?php

    $objBlog = new Blog();
    $objBlog->setData($_GET);
    $oneData = $objBlog->view("obj");

    ?>
    <div class="col-md-8">
        <h3 style="color: #ed2323"><?php echo $oneData->title;; ?></h3>

        <p class="para" align="left">Reported by&nbsp;<b><?php echo $oneData->author; ?></b>
            at&nbsp;<b><?php echo $oneData->created_at; ?></b></p>

        <h3 style="color: #ed2323">Images</h3>


        <img class="img-responsive" src="../../resource/images/blog/<?php echo $oneData->image; ?>">

        <br>
        
	<!-- Facebook share -->
	<!-- Load Facebook SDK for JavaScript -->
   <div id="fb-root"></div>
   <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

	<!-- Your share button code -->
	<div  style="float:right" class="fb-share-button" data-href="http://localhost/FixMyArea_BITM_FINAL_PROJECT/views/IIUC_ULTRON/blog-single.php?id=<?php echo $oneData->id; ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Flocalhost%2FFixMyArea_BITM_FINAL_PROJECT%2Fviews%2FIIUC_ULTRON%2Fsingle_view.php%3Fid%3D<?php echo $oneData->id; ?>&amp;src=sdkpreparse">Share</a></div>
	<!-- Facebook share -->
	
		
	
    <p><?php echo $oneData->title; ?></p>
    </div>


    <!---->


    <!--============Start of comments=========== -->
    <div class="col-md-4">
        <h3 style="color: #4CAF50;"> Comments Section: </h3>


		<?php
		$url = (!empty($_SERVER['HTTPS'])) ? 
		'https://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : 
		'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
		?>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <div style="" class="fb-comments" data-href="<?php echo $url; ?>"
             data-width="25%" data-numposts="5"></div>
    </div>
    <!--============End of comments=========== -->

</div><!-- end main -->
<div class="footer_bg"><!-- start footer -->
    <div class="container">
        <div class="row  footer">
            <div class="copy text-center">
                <p class="link"><span>&copy; Developed by <a href="http://facebook.com/fahim.iftekhar.5">Iftekharul Islam</span></p>
            </div>
        </div>
    </div>
</div>




</body>
</html>
