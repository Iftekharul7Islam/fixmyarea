  <style>

  .footer-p {font-size: 16px;}

  .bg-4 { 
      background-color: #2f2f2f; /* Black Gray */
      color: #fff;
  }
  .container-fluid {
      padding-top: 5px;
      padding-bottom: 5px;
  }
  .footer-li{
    font-size: 17px;
  }
  .about{
    margin-left: 13%;
    text-align: justify;

  }


  </style>

<footer class="container-fluid bg-4">
  <div class="row">
    <div class="col-sm-6">
    <div class="about">
      <h2 style="color: white">About Us</h2>
      <p class="footer-p">Clean City is an initiative to resolve civic life issues through citizens participation. It is a medium to connect the citizens directly with City Corporation to work on the common issues like sanitation, waste management, problems with roads and streets, sewerage system etc of their respective areas. </p>
    </div>
    </div>
    <div class="col-sm-3">
    <h2>Links</h2>
    <ul>
      <li class="footer-li"><a href="associate-volunteers.php">Associate Volunteer</a></li>
      <li class="footer-li"><a href="admin/admin_index.php">Volunteer Login</a></li>
    </ul>
    </div>
    <div class="col-sm-3">
    <h2>Follow Us</h2>
      <ul>
        <li class="footer-li"><a href="#">facebook</a></li>
        <li class="footer-li"><a href="#">Twitter</a></li>
      </ul>
    </div>
   </div>
</footer>