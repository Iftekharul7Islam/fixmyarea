<div class="row h_menu">
    <nav class="navbar navbar-default navbar-left" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
         <ul class="nav navbar-nav">
            <li><a href="index.php">Home</a></li>
            <li><a href="problems.php">View Problems</a></li>
            <li class=""><a href="all_reports.php">All Reports </a></li>
            <li><a href="fixed_problems.php">Recently Fixed Problems</a></li>
            <li><a href="report_a_problem.php">Report a Problem</a></li>
            <li><a href="blog_all.php">Blog</a></li>
            <li><a href="profile/join_us.php">Join Us</a></li>
              <!--if the user is logged in then logout will be shown otherwise login will be shown-->
              <?php if (!isset($_SESSION['email'])) { ?>
                        <li><a href="profile/signup_login.php">Login</a></li>
                        <!-- <li><a href="contact.php">Contact Us</a></li> -->
                    <?php } else { ?>
                        <li><a href="profile/profile.php">Profile</a></li>
                        <li><a href="profile/logout.php">Log Out</a></li>
                    <?php } ?>
          </ul>
        </div><!-- /.navbar-collapse -->
        <!-- start soc_icons -->
    </nav>
    <div class="soc_icons navbar-right">
            
    </div>
</div>