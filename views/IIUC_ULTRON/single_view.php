<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
require_once("../../vendor/autoload.php");
use App\Reports\Reports;
use App\Message\Message;

if (!isset($_SESSION)) session_start();
$_GLOBAL = Message::message();
?>

<!DOCTYPE HTML>
<html>
<head>
    <title>CleanCity</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="../../resource/css/bootstrap.css" rel='stylesheet' type='text/css'/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        } 
    </script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="../../resource/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    

    <!----font-Awesome----->
    <link rel="stylesheet" href="../../resource/fonts/css/font-awesome.min.css">
    <!----font-Awesome----->
    <link rel="stylesheet" href="../../resource/css/flexslider.css" type="text/css" media="screen"/>
  <!-- Scroll to top-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://arrow.scrolltotop.com/arrow7.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" rel="stylesheet" type="text/css" media="all"/>
    <noscript>Not seeing a <a href="http://www.scrolltotop.com/">Scroll to Top Button</a>? Go to our FAQ page for more info.</noscript>
  <!-- Scroll to top End-->

	<!--Facebook share -->
    <meta property="og:url"           content="http://localhost/FixMyArea_BITM_FINAL_PROJECT/views/IIUC_ULTRON/index.php" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="FixMyArea" />
	<meta property="og:description"   content="description" />
	<meta property="og:image"         content="" />
<!--Facebook share -->


</head>
<body>
<div class="header_bg1">
    <div class="container">
        <div class="row header">
            <div class="logo navbar-left">
                <h1><a href="index.php">Clean City</a></h1>
            </div>
            <div class="h_search navbar-right">
                <form id="searchForm" action="problems.php" method="get">
                    <input type="hidden" name="area" id="inlineCheckbox1" checked="" value="">
                    <input type="hidden" name="ward_no" id="inlineCheckbox1" checked="" value="">
                    <input type="text" id="searchID" name="search" class="text" value="Search by Area Name or Ward No"
                           onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search by Area Name or Ward No';}">
                    <input type="submit" value="search">
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
        <?php include('header.php'); ?>
        <div class="clearfix"></div>
    </div>
</div>
<div class="container"><!-- start main -->

    <!---->
    <?php

    $objReports = new Reports();
    $objReports->setData($_GET);
    $oneData = $objReports->view("obj");

    ?>
    <div class="row">
      <div class="col-md-8">
        
        <h3 style="color: #ed2323"><?php echo $oneData->title;; ?></h3>

        <p class="para" align="left">Reported by&nbsp;<b><?php echo $oneData->author; ?></b>
            at&nbsp;<b><?php echo $oneData->date; ?></b></p>
        <p class="para" align="left">Area:&nbsp;&nbsp;<b><?php echo $oneData->area; ?></b>, Word
            no:&nbsp;&nbsp;<b><?php echo $oneData->ward_no; ?></b></p>
        <p class="para" align="left">Category:&nbsp;&nbsp;<b><?php echo $oneData->category; ?></b></p>
        <h4 class="">Description: <?php echo $oneData->description; ?></h4>
        <h3 style="color: #ed2323">Images</h3>

        <div class="w3-content w3-display-container">
          <img class="mySlides" src="../../resource/images/images/<?php echo $oneData->image_1; ?>" height="300px" width="700px">
          <img class="mySlides" src="../../resource/images/images/<?php echo $oneData->image_2; ?>" height="300px" width="700px">
          <img class="mySlides" src="../../resource/images/images/<?php echo $oneData->image_3; ?>" height="300px" width="700px">

          <button class="w3-button w3-black w3-display-left btn btn-success btn-lg" onclick="plusDivs(-1)">&#10094;</button>
          <button class="w3-button w3-black w3-display-right btn btn-success btn-lg" onclick="plusDivs(1)">&#10095;</button>
        </div>

        <script>
        var slideIndex = 1;
        showDivs(slideIndex);

        function plusDivs(n) {
          showDivs(slideIndex += n);
        }

        function showDivs(n) {
          var i;
          var x = document.getElementsByClassName("mySlides");
          if (n > x.length) {slideIndex = 1}    
          if (n < 1) {slideIndex = x.length}
          for (i = 0; i < x.length; i++) {
             x[i].style.display = "none";  
          }
          x[slideIndex-1].style.display = "block";  
        }
        </script>


        <?php if (!empty($oneData->video)) { ?>
            <h3 style="color: #ed2323">Video</h3>

            <video align="center" width="100%" controls>
                <source src="../../resource/images/images/<?php echo $oneData->video; ?>" type="video/mp4">
                Your browser does not support HTML5 video.
            </video>
        <?php } ?>

        <br>
	<!-- Facebook share -->
	<!-- Load Facebook SDK for JavaScript -->
   <div id="fb-root"></div>
   <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

	<!-- Your share button code -->
	<div  style="float:right" class="fb-share-button" data-href="http://localhost/FixMyArea_BITM_FINAL_PROJECT/views/IIUC_ULTRON/single_view.php?id=<?php echo $oneData->id; ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Flocalhost%2FFixMyArea_BITM_FINAL_PROJECT%2Fviews%2FIIUC_ULTRON%2Fsingle_view.php%3Fid%3D<?php echo $oneData->id; ?>&amp;src=sdkpreparse">Share</a></div>
	<!-- Facebook share -->
	   
        <h3><b style="color: #ed2323">Admin Feedback:&nbsp;&nbsp;</b><?php echo $oneData->admin_feedback; ?></h3><br>
		
		<h3><b style="color: #ed2323">Location Map :&nbsp;&nbsp;</b></h3><br>
        <div id="map"></div>

    </div>


    <!---->
   

    <!--============Start of comments=========== -->
    <div class="col-md-4">
        <h3 style="color: #4CAF50;"> Comments Section: </h3>


		<?php
		$url = (!empty($_SERVER['HTTPS'])) ? 
		'https://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : 
		'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
		?>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <div style="" class="fb-comments" data-href="<?php echo $url; ?>"
             data-width="25%" data-numposts="5"></div>
    </div>
    <!--============End of comments=========== -->
     </div>
</div><!-- end main -->
<div class="footer_bg"><!-- start footer -->
    <div class="container">
        <div class="row  footer">
            <div class="copy text-center">
                <p class="link"><span>&copy; Developed by <a href="http://facebook.com/fahim.iftekhar.5">Iftekharul Islam</span></p>
            </div>
        </div>
    </div>
</div>


<script>
    function initMap() {
        var location = {lat: <?php echo $oneData->latitude; ?>, lng: <?php echo $oneData->longitude; ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: location
        });
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
    }
</script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCposPSTISJX8iGplo48fQIyapyySAO5AU&callback=initMap">
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>
<script type="text/javascript">
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        }
    }
})
</script>


</body>
</html>
